FROM node:8.9.1-alpine

WORKDIR /var/www/html
COPY . /var/www/html

RUN npm install

CMD ["npm", "start"]
