const mongoose = require('mongoose');

const schema = new mongoose.Schema(
  {
    text: {
      type: String
    },
    done: {
      type: Boolean,
      default: false
    },
    userId: {
      type: mongoose.SchemaTypes.ObjectId
    }
  }
);

module.exports = mongoose.model('todo', schema);
