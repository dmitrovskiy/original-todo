const mongoose = require('mongoose');

const schema = new mongoose.Schema(
  {
    firstName: {
      type: String
    },
    lastName: {
      type: String
    },
    email: {
      type: String,
      required: true
    }
  }
);

module.exports = mongoose.model('user', schema);
