const createService = require('feathers-mongoose');
const User = require('../../models/user');

module.exports = function () {
  const app = this;
  const options = { 
    Model: User
  };

  app.use('/users', createService(options));
};
