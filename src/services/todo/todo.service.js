const createService = require('feathers-mongoose');
const Todo = require('../../models/todo');

module.exports = function () {
  const app = this;
  const options = { 
    Model: Todo
  };

  app.use('/todos', createService(options));
};
