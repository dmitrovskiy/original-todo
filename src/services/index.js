const mongoose = require('mongoose');
const todo = require('./todo/todo.service.js');
const user = require('./user/user.service.js');

module.exports = function () {
  const app = this;
  
  mongoose.connect(app.get('database'));

  app.configure(todo);
  app.configure(user);
};
